import app from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDAEUaNiFtvRCTeFxrZUI5LQXrgoBapHFE",
    authDomain: "home-fbe0c.firebaseapp.com",
    databaseURL: "https://home-fbe0c.firebaseio.com",
    projectId: "home-fbe0c",
    storageBucket: "home-fbe0c.appspot.com",
    messagingSenderId: "121094561857",
    appId: "1:121094561857:web:9acc54bf0522a1d4d7e6c0",
    measurementId: "G-BZDGZD9S0Z"
  };

class Firebase{

    constructor(){
        app.initializeApp(config);
        this.db = app.firestore();
        this.auth = app.auth();

    }

    estaIniciado(){
        return new Promise(resolve => {
            this.auth.onAuthStateChanged(resolve);
        })
    }
}

export default Firebase;